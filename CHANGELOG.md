# Change Log for Lakeshore Model 336 module
All notable changes to `Lakeshore Model 336` will be documented in this file.

## 0.3.0 - 2024-11-07
First version able to use all functions of the `Lakeshore Modell 336` except controller cards

### Updated/Changed
 - Compatiblity with version 0.3 of Temperature System Server

## 0.2.1 - 2024-04-30

### Fixed
 - Initialsation and readout of the temperature sensors
 - Get parameter values from the device
 - Log exceptions during readout


## 0.2.0 - 2024-04-22
First Early Access Version released

