# Lakeshore Model 336 module
Version 0.3.0

Module to control the `Lakeshore Model 336` with the [Temperature System Server](https://gitlab.mpcdf.mpg.de/thschmidt/temperature-system-server).

## Supported Connections
- USB
- LAN (Ethernet)

## Supported Cards
Currently no extension is supported

## Resources
 - [Product Page](https://www.lakeshore.com/products/categories/overview/temperature-products/cryogenic-temperature-controllers/model-336-cryogenic-temperature-controller)

