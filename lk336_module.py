
###############################################################################
# IMPORTS
###############################################################################

from lakeshore import Model336
from lakeshore import Model336InputSensorSettings

###############################################################################
# CLASS
###############################################################################

class LK336:
    def __init__(self, module_basics):
        self._mob = module_basics
        
        # Variable to save connection link of device
        self.inst = ''
        
        self.heater_factors = {'OFF':0, 'LOW':0.01, 'MEDIUM':0.1, 'HIGH':1}
        
    """------------------------- INITIAL FUNCTIONS -------------------------""" 

    def init_device(self, setup=False):
        """Initialize the device parameters"""
        if not setup:
            # Initial readout
            device_channels = self.device_body['Channel']
            # Get heater / output parameters
            for channel in device_channels:
                channel_data = self.get_heater_parameters(device_channels[channel], 
                                                          channel[-1],
                                                          init_set_parameter=True)
                self.device_body['Channel'][channel] = channel_data

        
    """----------------------- CONNECTION FUNCTIONS -----------------------"""
    
    def open_connection(self, device_connection={}):
        """
        Open Connection to device . 
        'device_connection': the connection info:
            'TYPE': 'Ethernet/USB/Serial,
            'IP/SerialNumber/Port':'XX.XX.XX.XX/XXXXXX/COMX
        """
        # Check if connection for type exists
        if device_connection['Type'] in ['Ethernet', 'USB']:
            if device_connection['Type'] == 'USB':
                self.inst = Model336(serial_number = device_connection['SerialNumber'])
            elif device_connection['Type'] == 'Ethernet':
                self.inst = Model336(ip_address = device_connection['IP'])
            self.check_connection()
        else:
            raise Exception("Only connections via ethernet or USB are supported")

    def close_connection(self, device_connection={}):
        """
        Close Connection to device . 
        'device_connection': the connection:
            'TYPE': 'Ethernet/USB/Serial,
            'IP/SerialNumber/Port':'XX.XX.XX.XX/XXXXXX/COMX
        """
        if device_connection == {}:
            device_connection = self.device_body['Connection']
            
        if device_connection['Type'] == 'USB':
            self.inst.disconnect_usb()
        elif device_connection['Type'] == 'Ethernet':
            self.inst.disconnect_tcp()
         
    def check_connection(self):
        """Check the connection to the device"""
        self.inst.query('*IDN?')
    
    """--------------------- SET AND REQUEST FUNCTIONS ---------------------"""
    
    def request_status(self, device_state, log_request):
        """
        Request the current status of the device (only during an active campaign)
        """
        self.readout_device(device_state, log_request)  
             
        
    def set_parameter(self, channel, parameter, value):
        """Send value of parameter of selected channel to device"""
        
        # Create a shorten variable
        channel_settings = self.device_body['Channel'][channel]
        

        if parameter in ['P', 'I', 'D']:
            # Fetch all Set PID settings
            p_value = []
            for p in ['P','I','D']:
                if parameter == p:
                    p_value.append(value)
                else:
                    p_value.append(channel_settings[p]['Set'])
            parameter = 'PID'
        elif 'Limit' in parameter:
            channel_settings[parameter]['Read'] = value
            parameter, p_value = self.change_limits(channel, parameter, value)
        elif parameter == 'Heater_Resistance':
            parameter, p_value = self.change_limits(channel, parameter, value)
        elif parameter in ['Mode', 'Input']: 
            if parameter == 'Mode':
                p_value = [value, channel_settings['Input']['Read']]
            else:
                p_value = [channel_settings['Mode']['Read'], value]
            parameter = 'Heater_Mode'
            channel_settings['Manual_Output']['Set'] = 0
        else:
            p_value = value
            
        
        # Send heater parameter to device
        self.set_heater_parameters(channel[-1],
                                   parameter,
                                   p_value)
                

    """------------------------ CAMPAIGN FUNCTIONS ------------------------"""
    
    def start_campaign(self):
        """Start a new campaign with last init settings"""
        pass
  
    def stop_campaign(self):
        """Start current campaign"""
        pass
                
    def init_campaign(self, campaign_details):
        """Initialize a new campaign"""
        # Configure temperature sensors of device
        self.configure_setup(self._mob.campaign_sensors)
        
    """--------------------- CAMPAIGN PREPARE FUNCTIONS --------------------""" 

    def configure_setup(self, sens_dict):
        """Configure the selected temperature sensors"""
        for sensor in sens_dict:
            sensor_type_list = sens_dict[sensor]['TYPE'].split('-')
            sensor_parameters = []
            sensor_type = sensor_type_list[0] # Get sensor type
            # Automatically set compensation ON (True),
            # only OFF (False) for Diodes
            compensation = True
            if sensor_type == 'Diode':
                compensation = False
            # Create sensor parameters
            sensor_parameters.append(getattr(self.inst.InputSensorType, 
                                     sensor_type.upper()))
            sensor_parameters.append(False) # OFF
            sensor_type_options = self.device_body['SensorTypes'][sensor_type]
            for key in sensor_type_options:
                # Get selected resistance (RTD, NTD) or voltage (Diode)
                rang = sensor_type_options[key].index(int(sensor_type_list[1]))
            sensor_parameters.append(compensation)
            sensor_parameters.append(1) # Kelvin
            sensor_parameters.append(rang)
            # Send everything to Model336
            self.set_temperature_sensors(sensor, sensor_parameters)

                    
    """--------------------------- SET FUNCTIONS ---------------------------""" 
    
    def set_heater_parameters(self, output, parameter, value):
        """Set / change parameters of heater for a selected output"""
        if parameter == 'PID':
            # Value includes P, I and D
            self.inst.set_heater_pid(output, *value)
        elif parameter == 'Set_Point':
            # Set temperature set point
            self.inst.set_control_setpoint(output, value)
        elif parameter == 'Heater_Range':
            if int(output) in [1,2]:
                set_value = getattr(self.inst.HeaterRange, value)
            else:
                set_value = getattr(self.inst.HeaterVoltageRange,
                                    'VOLTAGE_' + value)
            self.inst.set_heater_range(output, set_value)
        elif parameter == 'Heater_Ramp':
            # Set ramp of heater (only for output 1 and 2)
            # value == 0, disables the ramp
            ramp_enable = True
            if value == '0':
                ramp_enable = False  
            self.inst.set_setpoint_ramp_parameter(output, ramp_enable, value)
        elif parameter == 'Heater_Setup':
            # if int(output) in [1,2]:
            #     # Value includes Resistance and max current in A, 
            #     # last parameter defines the heater output as Power
            user_resistance = value[0]
            max_current = value[1]
            if float(user_resistance) < 50:
                resistance = 1
            else:
                resistance = 2
            output_display = 1 # Display Current
            self.inst.set_heater_setup(output, resistance,
                                       max_current, output_display)
            # else:
            #     # Value includes Sensor Number, Temperature Type,
            #     # limits and polarity
                # inst.set_monitor_output_heater(output, *value)
        elif parameter == 'Heater_Mode':
            # Combine heater (output) with sensor (input) by the selected mode
            # Manual output is reset to 0
            self.inst.set_heater_output_mode(output,
                                            self.inst.HeaterOutputMode[value[0]],
                                            value[1])
        elif parameter == 'Manual_Output':
            # Set the output value manually in %
            self.inst.set_manual_output(output, value)


    def set_temperature_sensors(self, sensor, sensor_parameters):
        self.inst.set_input_sensor(sensor,
                                   Model336InputSensorSettings(*sensor_parameters))
    
    def set_temperature_limit(self, sensor, limit):
        self.inst.set_temperature_limit(sensor, limit)
        
  

    """------------------------- READOUT FUNCTIONS -------------------------""" 

    def readout_device(self, device_state, readout_sensor=False):
        """Readout data from Model336. On request readout temperature data"""
        if device_state in ['Online', 'Busy']: 
            # Get current time
            timestamp = self._mob.create_timestamp()
            self.device_body['LastReadout'] = timestamp
            device_channels = self.device_body['Channel']
            # Get heater / output parameters
            for channel in device_channels:
                channel_data = self.get_heater_parameters(device_channels[channel], 
                                                          channel[-1])
                self.device_body['Channel'][channel] = channel_data
            self.device_body['Channel'] = device_channels
            if readout_sensor and device_state == 'Busy':
                # Readout the sensors if campaign active and requested
                temp_data = self.get_temperatures()
                self.device_body['SensorData'] = {timestamp:temp_data}


    def get_heater_parameters(self, channel_data, output, init_set_parameter=False):
        """Get all heater parameters and convert them"""
        for var in ['Read', 'Set']:
            if 'Mode' in channel_data:
                out_mode = self.inst.get_heater_output_mode(output)
                channel_data['Mode'][var] = out_mode['mode'].name
                channel_mode = out_mode['channel'].name
                if channel_mode == 'NONE':
                    channel_data['Input'][var] = channel_mode
                else:
                    channel_data['Input'][var] = channel_mode.split('_')[1]
            if 'Heater_Resistance' in channel_data:
                heater_setup = self.inst.get_heater_setup(output)
                set_resistance = channel_data['Heater_Resistance']['Set'] 
                channel_data['Heater_Resistance']['Read'] = set_resistance
                if var == 'Read':
                    channel_data['Max_Current'][var] = heater_setup['max_current']
            if 'Manual_Output' in channel_data:
                # Get manual output in %
                channel_data['Manual_Output'][var] = self.inst.get_manual_output(output)
            if 'Heater_Ramp' in channel_data:
                # Get heater ramp (rate_value)
                ramp = self.inst.get_setpoint_ramp_parameter(output)['rate_value']
                channel_data['Heater_Ramp'][var] = ramp
            if 'Heater_Range' in channel_data:
                # Get heater range
                pm = 'Heater_Range' # Parameter
                rang = int(self.inst.get_heater_range(output))
                channel_data[pm][var] = channel_data[pm]['Values'][rang]
            if 'Set_Point' in channel_data:
                # Get Set Point
                channel_data['Set_Point'][var] = self.inst.get_control_setpoint(output)
            if 'P' in channel_data:
                # PID
                pid = self.inst.get_heater_pid(output)
                channel_data['P'][var] = pid['gain']
                channel_data['I'][var] = pid['integral']
                channel_data['D'][var] = pid['ramp_rate']
            if 'Current' in channel_data:
                # Get
                percentage = self.inst.get_heater_output(output)
                max_current = float(channel_data['Max_Current']['Read'])
                heater_range = channel_data['Heater_Range']['Read']
                factor = self.heater_factors[heater_range]
                current = percentage/100 * max_current * factor
                channel_data['Current']['Read'] = round(current, 3)
                if 'Power' in channel_data:
                    resistance = channel_data['Heater_Resistance']['Set']
                    channel_data['Power']['Read'] = round(current**2 * float(resistance),3)
            if 'Voltage' in channel_data:
                # Readout voltage of the analog output in percent
                percentage = float(self.inst.query(f"AOUT? {output}"))
                # Voltage 0-10V is 0-100%,
                channel_data['Voltage']['Read'] = percentage / 10
 
            if not init_set_parameter:
                break
                
        if init_set_parameter:
            # Initial set of user limits
            for parameter in ['Limit_Voltage','Limit_Current','Limit_Power']:
                if parameter in channel_data:
                    if str(channel_data[parameter]['Set']) == '0':
                        channel_data[parameter]['Set'] = channel_data[parameter]['Limits'][1]
          
            
        return channel_data
        

    def get_temperatures(self):
        """Get the temperatures of the active sensors"""
        temp_data = {}
        for sens in self._mob.campaign_sensors.keys():
            temp_data[sens] = self.inst.get_kelvin_reading(sens)
        return temp_data
    
    """--------------------------- SUBFUNCTIONS ---------------------------"""
    
    def change_limits(self, channel, parameter, value):
        """
        Change the limit of the maximum current depending on the
        voltage, power, resistance, ...
        """
        channel_settings = self.device_body['Channel'][channel]
        output = channel[-1]
        if int(output) in [1,2]: 
            # Get all needed parameters (resistance, current, ...)
            limits = {'Heater_Resistance':0, 'Limit_Power':0,'Limit_Voltage':0, 'Limit_Current':0}
            for p in limits.keys():
                if parameter == p:
                    limits[p] = float(value)
                else:
                    limits[p] = float(channel_settings[p]['Set'])

            if limits['Heater_Resistance'] > 0 and limits['Limit_Power'] > 0:
                # Find the lowest current allowed
                current_limit = self.calculate_current_limit(limits['Limit_Power'], 
                                                             limits['Heater_Resistance'],
                                                             limits['Limit_Voltage'])
                max_current = round(min(limits['Limit_Current'], current_limit), 3)
                # Update the channel dictionary
                channel_settings['Max_Current']['Read'] = max_current
                
                p_value = [limits['Heater_Resistance'], max_current]
            else:
                p_value = [value, channel_settings['Max_Current']['Read']]
        parameter = 'Heater_Setup'
        return parameter, p_value
                
    def calculate_current_limit(self, power, resistance, voltage):
        """Find the maximum current allowed for this configuration"""
        # Power Limit
        i1 = (power/resistance)**0.5 
        # Voltage Compliance Limit
        i2 = voltage/resistance
        max_current = min(i1,i2)
        return max_current


